require('dotenv-flow').config()

const mongoose = require('mongoose')

beforeAll(async () => {
  return mongoose
    .connect(process.env.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true })
})
afterAll(async () => {
  await mongoose.disconnect
})
