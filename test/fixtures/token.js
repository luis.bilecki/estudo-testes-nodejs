require('dotenv-flow')

const jwtService = require('../../src/services/jwt')

module.exports = {
  validToken: jwtService.sign('luis@luis.com'),
  invalidToken: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1OTQ5MDU4MDgsImV4cCI6MTYyNjQ0MTgwOCwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoibHVpc0BleGFtcGxlLmNvbSJ9.DrIzajgSmKEtArjPJGXq_4qaXFLAiO3lXyaUjJTPwgQ'
}
