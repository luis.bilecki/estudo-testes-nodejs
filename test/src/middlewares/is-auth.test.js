const httpMocks = require('node-mocks-http')

const token = require('../../fixtures/token')

const isAuthMiddleware = require('../../../src/middlewares/is-auth')

describe('middlewares > is-auth', () => {
  test('Should call next with unauthorized error when token is not found', () => {
    const req = httpMocks.createRequest({
      method: 'GET',
      url: '/locales',
      headers: {}
    })
    const res = jest.fn()
    const next = jest.fn()

    isAuthMiddleware(req, res, next)

    const nextArgument = next.mock.calls[0][0]
    expect(nextArgument.errorName).toEqual('Unauthorized')
    expect(nextArgument.status).toEqual(401)
  })

  test('Should call next with unauthorized error when token is invalid', () => {
    const req = httpMocks.createRequest({
      method: 'GET',
      url: '/locales',
      headers: {
        authorization: `Bearer ${token.invalidToken}`
      }
    })
    const res = jest.fn()
    const next = jest.fn()

    isAuthMiddleware(req, res, next)

    const nextArgument = next.mock.calls[0][0]
    expect(nextArgument.errorName).toEqual('Unauthorized')
    expect(nextArgument.status).toEqual(401)
  })

  test('Should call next with unauthorized error when token can not be decode', () => {
    const req = httpMocks.createRequest({
      method: 'GET',
      url: '/locales',
      headers: {
        authorization: 'Bearer'
      }
    })
    const res = jest.fn()
    const next = jest.fn()

    isAuthMiddleware(req, res, next)

    const nextArgument = next.mock.calls[0][0]
    expect(nextArgument.errorName).toEqual('Unauthorized')
    expect(nextArgument.status).toEqual(401)
  })

  test('Should update req with userEmail when token is valid', () => {
    const req = httpMocks.createRequest({
      method: 'GET',
      url: '/locales',
      headers: {
        authorization: `Bearer ${token.validToken}`
      }
    })
    const res = jest.fn()
    const next = jest.fn()

    isAuthMiddleware(req, res, next)

    expect(next).toBeCalled()
    expect(req.userEmail).toBeDefined()
  })
})
