const faker = require('faker')

const LocaleModel = require('../../../src/models/locale')

describe('models > Locale', () => {
  describe('validate', function () {
    test('Should return an error when name is empty', function () {
      const locale = new LocaleModel({
        name: '',
        geometry: { type: 'Point', coordinates: [0, 0] }
      })
      const validation = locale.validateSync()

      expect(validation.errors.name.message).toEqual('Path `name` is required.')
    })

    test('Should return an error when geometry type is not equals to "Point"', function () {
      const locale = new LocaleModel({
        name: faker.address.city(),
        geometry: {
          type: 'Polygon',
          coordinates: [0, 0]
        }
      })
      const validation = locale.validateSync()

      expect(validation.errors['geometry.type'].message).toEqual('`Polygon` is not a valid enum value for path `geometry.type`.')
    })

    test('Should not return errors when required data is provided', function () {
      const locale = new LocaleModel({
        name: faker.address.city(),
        geometry: {
          type: 'Point',
          coordinates: [0, 0]
        }
      })
      const validation = locale.validateSync()

      expect(validation).toBeUndefined()
    })
  })

  test('Should not save with invalid data', async function () {
    const locale = new LocaleModel({
      name: faker.address.city(),
      geometry: {
        type: 'Polygon',
        coordinates: [0, 0]
      }
    })

    try {
      await locale.save()
    } catch (err) {
      expect(err).toBeDefined()
      expect(err.message).toEqual('Locale validation failed: geometry.type: `Polygon` is not a valid enum value for path `geometry.type`.')
    }
  })

  test('Should save locale on database', async function () {
    const locale = new LocaleModel({
      name: faker.address.city(),
      geometry: {
        type: 'Point',
        coordinates: [
          faker.address.longitude(),
          faker.address.latitude()
        ]
      }
    })

    await locale.save()

    expect(locale._id).toBeDefined()
  })
})
