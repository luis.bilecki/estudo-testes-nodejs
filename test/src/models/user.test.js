const faker = require('faker')

const UserModel = require('../../../src/models/user')

describe('models > User', () => {
  describe('validate', function () {
    test('Should return an error when email is empty', function () {
      const user = new UserModel({ email: '', password: faker.internet.password() })
      const validation = user.validateSync()

      expect(validation.errors.email.message).toEqual('Path `email` is required.')
    })

    test('Should return an error when password is empty', function () {
      const user = new UserModel({ email: faker.internet.email(), password: '' })
      const validation = user.validateSync()

      expect(validation.errors.password.message).toEqual('Path `password` is required.')
    })

    test('Should not return errors when required data is provided', function () {
      const user = new UserModel({ email: faker.internet.email(), password: faker.internet.password() })
      const validation = user.validateSync()

      expect(validation).toBeUndefined()
    })
  })

  describe('save', function () {
    test('Should hash user password', async function () {
      const password = faker.internet.password()
      const user = new UserModel({
        email: faker.internet.email(),
        password
      })

      await user.save()

      expect(user.password).not.toEqual(password)
    })
  })

  describe('comparePassword', function () {
    const password = faker.internet.password()
    let defaultUser = null

    beforeAll(async () => {
      defaultUser = new UserModel({
        email: faker.internet.email(),
        password
      })

      await defaultUser.save()
    })

    test('Should return true when password is equal', function () {
      const passwordMatch = defaultUser.comparePassword(password)
      expect(passwordMatch).toBeTruthy()
    })

    test('Should return false when password is not equal', function () {
      const passwordMatch = defaultUser.comparePassword('luis')
      expect(passwordMatch).toBeFalsy()
    })
  })
})
