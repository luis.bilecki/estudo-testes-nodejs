const app = require('../../../src/app')

const supertest = require('supertest')
const request = supertest(app)
const faker = require('faker')

const tokenFixture = require('../../fixtures/token')

const UserModel = require('../../../src/models/user')

describe('controllers > Auth', () => {
  describe('POST /auth/signin', () => {
    const password = faker.internet.password()
    let defaultUser = null

    beforeAll(async () => {
      defaultUser = new UserModel({
        email: faker.internet.email(),
        password
      })

      await defaultUser.save()
    })

    test('Should return an error when body is empty', async () => {
      const res = await request
        .post('/auth/signin')
        .set('Accept', 'application/json')
        .send({})

      expect(res.statusCode).toEqual(400)
    })

    test('Should return an error when password is empty', async () => {
      const res = await request
        .post('/auth/signin')
        .set('Accept', 'application/json')
        .send({
          email: faker.internet.email(),
          password: ''
        })

      expect(res.statusCode).toEqual(400)
      expect(res.body.timestamp).toBeDefined()
      expect(res.body.error).toEqual('Bad request')
      expect(res.body.message).toBeDefined()
      expect(res.body.message.errors.length).not.toEqual(0)
    })

    test('Should return an error when user is not found', async () => {
      const res = await request
        .post('/auth/signin')
        .set('Accept', 'application/json')
        .send({
          email: faker.internet.email(),
          password: faker.internet.password()
        })

      expect(res.statusCode).toEqual(404)
      expect(res.body.timestamp).toBeDefined()
      expect(res.body.error).toEqual('Not Found')
      expect(res.body.message).toEqual('User not found')
    })

    test('Should return an error when user password does not match', async () => {
      const res = await request
        .post('/auth/signin')
        .set('Accept', 'application/json')
        .send({
          email: defaultUser.email,
          password: faker.internet.password()
        })

      expect(res.statusCode).toEqual(401)
      expect(res.body.timestamp).toBeDefined()
      expect(res.body.error).toEqual('Unauthorized')
      expect(res.body.message).toEqual('Wrong password!')
    })

    test('Should return user token with successful sign in', async () => {
      const res = await request
        .post('/auth/signin')
        .set('Accept', 'application/json')
        .send({
          email: defaultUser.email,
          password: password
        })

      expect(res.statusCode).toEqual(200)
      expect(res.body.token).toBeDefined()
    })
  })

  describe('POST /auth/signup', () => {
    test('Should return an error when body is empty', async () => {
      const res = await request
        .post('/auth/signup')
        .set('Accept', 'application/json')
        .send({})

      expect(res.statusCode).toEqual(400)
      expect(res.body.timestamp).toBeDefined()
      expect(res.body.error).toEqual('Bad request')
      expect(res.body.message).toBeDefined()
    })

    test('Should return an error with weak password (min length < 8)', async () => {
      const res = await request
        .post('/auth/signup')
        .set('Accept', 'application/json')
        .send({
          email: faker.internet.email(),
          password: 'abc'
        })

      expect(res.statusCode).toEqual(400)
      expect(res.body.timestamp).toBeDefined()
      expect(res.body.error).toEqual('Bad request')
      expect(res.body.message).toBeDefined()
      expect(res.body.message.errors).toMatchObject([
        {
          value: 'abc',
          msg: 'Invalid value',
          param: 'password',
          location: 'body'
        }
      ])
    })

    test('Should return success when a user is registered', async () => {
      const res = await request
        .post('/auth/signup')
        .set('Accept', 'application/json')
        .send({
          email: faker.internet.email(),
          password: faker.internet.password()
        })

      expect(res.statusCode).toEqual(200)
      expect(res.body.success).toBeTruthy()
    })
  })

  describe('POST /auth/validate-token', () => {
    test('Should return an error when body is empty', async () => {
      const res = await request
        .post('/auth/validate-token')
        .set('Accept', 'application/json')
        .send({})

      expect(res.statusCode).toEqual(400)
      expect(res.body.timestamp).toBeDefined()
      expect(res.body.error).toEqual('Bad request')
      expect(res.body.message).toBeDefined()
    })

    test('Should return an error when token is not valid', async () => {
      const res = await request
        .post('/auth/validate-token')
        .set('Accept', 'application/json')
        .send({
          token: tokenFixture.invalidToken
        })

      expect(res.statusCode).toEqual(401)
      expect(res.body.timestamp).toBeDefined()
      expect(res.body.error).toEqual('Unauthorized')
      expect(res.body.message).toEqual('Invalid token')
    })

    test('Should return success with a valid token', async () => {
      const res = await request
        .post('/auth/validate-token')
        .set('Accept', 'application/json')
        .send({
          token: tokenFixture.validToken
        })

      expect(res.statusCode).toEqual(200)
      expect(res.body.status).toEqual('valid')
    })
  })
})
