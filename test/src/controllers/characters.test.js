const request = require('supertest')

const app = require('../../../src/app')

const rickyAndMortyMock = require('../mocks/ricky-and-morty-api')

describe('controllers > Character', () => {
  test('Should return a list of characters', async () => {
    // Mock external http request
    const page = 1
    rickyAndMortyMock.mockCharacters(page)

    const res = await request(app)
      .get(`/characters?page=${page}`)
      .set('Accept', 'application/json')

    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('info')
    expect(res.body).toHaveProperty('results')
  })
})
