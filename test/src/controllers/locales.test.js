const request = require('supertest')
const faker = require('faker')

const tokenFixture = require('../../fixtures/token')

const app = require('../../../src/app')

const LocaleModel = require('../../../src/models/locale')

describe('controllers > Locale', () => {
  describe('As a user with valid token', () => {
    describe('GET /locales', () => {
      test('Should return a list of locales', async () => {
        const res = await request(app)
          .get('/locales')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)

        expect(res.statusCode).toEqual(200)
        expect(res.body).toEqual(expect.any(Array))
      })
    })

    describe('GET /locales/:id', () => {
      let defaultLocale

      beforeAll(async () => {
        defaultLocale = new LocaleModel({
          name: faker.address.city(),
          geometry: {
            type: 'Point',
            coordinates: [
              faker.address.latitude(),
              faker.address.longitude()
            ]
          }
        })

        await defaultLocale.save()
      })

      test('Should return an error when id is invalid', async () => {
        const res = await request(app)
          .get('/locales/123')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)

        expect(res.statusCode).toEqual(400)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Bad request')
        expect(res.body.message.errors.length).not.toEqual(0)
      })

      test('Should return an error when id not exists', async () => {
        const res = await request(app)
          .get('/locales/5f107759c2747aabc6d8b068')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)

        expect(res.statusCode).toEqual(404)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Not Found')
        expect(res.body.message).toEqual('Locale not found')
      })

      test('Should return locale data', async () => {
        const { _id, name, geometry } = defaultLocale.toJSON()
        const res = await request(app)
          .get(`/locales/${_id}`)
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)

        expect(res.statusCode).toEqual(200)
        expect(res.body).toMatchObject({
          _id: _id.toString(),
          name,
          geometry
        })
      })
    })

    describe('POST /locales', () => {
      test('Should return an error with invalid data', async () => {
        const res = await request(app)
          .post('/locales')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)
          .send({
            name: faker.address.city(),
            type: 'Polygon',
            coordinates: null
          })

        expect(res.statusCode).toEqual(400)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Bad request')
        expect(res.body.message.errors.length).not.toEqual(0)
      })

      test('Should create a locale register', async () => {
        const res = await request(app)
          .post('/locales')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)
          .send({
            name: faker.address.city(),
            type: 'Point',
            coordinates: [
              faker.address.latitude(),
              faker.address.longitude()
            ]
          })

        expect(res.statusCode).toEqual(200)
        expect(res.body._id).toBeDefined()
      })
    })

    describe('PUT /locales/:id', () => {
      let defaultLocale

      beforeAll(async () => {
        defaultLocale = new LocaleModel({
          name: faker.address.city(),
          geometry: {
            type: 'Point',
            coordinates: [
              faker.address.latitude(),
              faker.address.longitude()
            ]
          }
        })

        await defaultLocale.save()
      })

      test('Should return an error when id is invalid', async () => {
        const res = await request(app)
          .put('/locales/123')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)
          .send({})

        expect(res.statusCode).toEqual(400)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Bad request')
        expect(res.body.message.errors.length).not.toEqual(0)
      })

      test('Should return an error when required parameters are invalid', async () => {
        const res = await request(app)
          .put('/locales/5f107759c2747aabc6d8b068')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)
          .send({
            name: '',
            coordinates: ''
          })

        expect(res.statusCode).toEqual(400)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Bad request')
        expect(res.body.message.errors.length).not.toEqual(0)
      })

      test('Should return an error when locale is not found', async () => {
        const res = await request(app)
          .put('/locales/5f107759c2747aabc6d8b068')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)
          .send({
            name: faker.address.city(),
            coordinates: [
              faker.address.latitude(),
              faker.address.longitude()
            ]
          })

        expect(res.statusCode).toEqual(404)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Not Found')
        expect(res.body.message).toEqual('Locale not found')
      })

      test('Should update locale data', async () => {
        const dataToUpdate = {
          name: faker.address.city(),
          coordinates: [
            faker.address.latitude(),
            faker.address.longitude()
          ]
        }
        const res = await request(app)
          .put(`/locales/${defaultLocale._id}`)
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)
          .send(dataToUpdate)

        expect(res.statusCode).toEqual(200)
        expect(res.body.name).toEqual(dataToUpdate.name)
        expect(res.body.geometry).toMatchObject({
          type: 'Point',
          coordinates: [
            Number(dataToUpdate.coordinates[0]),
            Number(dataToUpdate.coordinates[1])
          ]
        })
      })
    })

    describe('DELETE /locales/:id', () => {
      let defaultLocale

      beforeAll(async () => {
        defaultLocale = new LocaleModel({
          name: faker.address.city(),
          geometry: {
            type: 'Point',
            coordinates: [
              faker.address.latitude(),
              faker.address.longitude()
            ]
          }
        })

        await defaultLocale.save()
      })

      test('Should return an error when id is invalid', async () => {
        const res = await request(app)
          .del('/locales/123')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)

        expect(res.statusCode).toEqual(400)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Bad request')
        expect(res.body.message.errors.length).not.toEqual(0)
      })

      test('Should return no content when locale is deleted', async () => {
        const _id = defaultLocale._id
        const res = await request(app)
          .del(`/locales/${_id}`)
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.validToken}`)

        expect(res.statusCode).toEqual(204)
      })
    })
  })

  describe('As a user with invalid token', () => {
    describe('GET /locales', () => {
      test('Should return an unauthorized error', async () => {
        const res = await request(app)
          .get('/locales')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.invalidToken}`)

        expect(res.statusCode).toEqual(401)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Unauthorized')
      })
    })

    describe('GET /locales/:id', () => {
      test('Should return an unauthorized error', async () => {
        const res = await request(app)
          .get('/locales/myid')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.invalidToken}`)

        expect(res.statusCode).toEqual(401)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Unauthorized')
      })
    })

    describe('POST /locales/', () => {
      test('Should return an unauthorized error', async () => {
        const res = await request(app)
          .post('/locales')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.invalidToken}`)
          .send()

        expect(res.statusCode).toEqual(401)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Unauthorized')
      })
    })

    describe('PUT /locales/:id', () => {
      test('Should return an unauthorized error', async () => {
        const res = await request(app)
          .put('/locales/1')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.invalidToken}`)
          .send()

        expect(res.statusCode).toEqual(401)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Unauthorized')
      })
    })

    describe('DELETE /locales/:id', () => {
      test('Should return an unauthorized error', async () => {
        const res = await request(app)
          .del('/locales/1')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${tokenFixture.invalidToken}`)
          .send()

        expect(res.statusCode).toEqual(401)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Unauthorized')
      })
    })
  })

  describe('As a guest', () => {
    describe('GET /locales', () => {
      test('Should return an unauthorized error', async () => {
        const res = await request(app)
          .get('/locales')
          .set('Accept', 'application/json')

        expect(res.statusCode).toEqual(401)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Unauthorized')
      })
    })

    describe('GET /locales/:id', () => {
      test('Should return an unauthorized error', async () => {
        const res = await request(app)
          .get('/locales/myid')
          .set('Accept', 'application/json')

        expect(res.statusCode).toEqual(401)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Unauthorized')
      })
    })

    describe('POST /locales/', () => {
      test('Should return an unauthorized error', async () => {
        const res = await request(app)
          .post('/locales')
          .set('Accept', 'application/json')
          .send()

        expect(res.statusCode).toEqual(401)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Unauthorized')
      })
    })

    describe('PUT /locales/:id', () => {
      test('Should return an unauthorized error', async () => {
        const res = await request(app)
          .put('/locales/1')
          .set('Accept', 'application/json')
          .send()

        expect(res.statusCode).toEqual(401)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Unauthorized')
      })
    })

    describe('DELETE /locales/:id', () => {
      test('Should return an unauthorized error', async () => {
        const res = await request(app)
          .del('/locales/1')
          .set('Accept', 'application/json')
          .send()

        expect(res.statusCode).toEqual(401)
        expect(res.body.timestamp).toBeDefined()
        expect(res.body.error).toEqual('Unauthorized')
      })
    })
  })
})
