## Estudos com testes usando Express e Mongo.db

Usado somente para fazer e estudar sobre testes automatizados.


## Requirements

* [Node >= 14](https://nodejs.org/en/) or [Docker](https://www.docker.com/)
* [Yarn](https://yarnpkg.com/)

## Usage

1. Clone this repository:

```
git clone https://gitlab.com/luis.bilecki/estudo-testes-nodejs.git
```

2. Build and create containers for services:

```
docker-compose up --build
```

3. To stop the running containers use:

```
Ctrl+C or docker-compose stop
```

4. After a build using "docker-compose up" the containers can be started using:

```
docker-compose start
```

If you do not use the docker and docker-compose, to run this project use the following steps.

1. Install all dependencies:

```
yarn or yarn install
```

2. To initialize the project for development, run:

```
yarn run start:dev
```

3. To initialize the project for production, run:

```
yarn run start
```

## Tests

This project uses the Jest library as test suite tool.
To run the tests use:

```
docker exec -it testes-api yarn test
```

_The containers must be running!_

OR

```
yarn test
```

## Linting

This project uses the ESLint with Standard.js as lint tool.
To run linting use:

```
docker exec -it testes-api yarn lint
```

_The containers must be running!_

OR

```
yarn lint
```
