const express = require('express')

const wrapAsync = require('../helpers/wrap-async')

const charactersController = require('../controllers/characters')
const router = express.Router()

router.get(
  '/',
  wrapAsync(charactersController.findAll)
)

module.exports = router
