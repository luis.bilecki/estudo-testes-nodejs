const express = require('express')

const wrapAsync = require('../helpers/wrap-async')

const validatorMiddleware = require('../middlewares/data-validator')

const authController = require('../controllers/auth')
const authValidations = require('../validations/auth-rules')

const router = express.Router()

router.post('/signup', authValidations.signUp, validatorMiddleware, wrapAsync(authController.signUp))
router.post('/signin', authValidations.signIn, validatorMiddleware, wrapAsync(authController.signIn))
router.post('/validate-token', authValidations.validateToken, validatorMiddleware, wrapAsync(authController.validateToken))

module.exports = router
