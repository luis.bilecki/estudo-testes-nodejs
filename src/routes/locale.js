const express = require('express')

const wrapAsync = require('../helpers/wrap-async')

const isAuthMiddleware = require('../middlewares/is-auth')
const validatorMiddleware = require('../middlewares/data-validator')

const localesController = require('../controllers/locales')
const localesValidation = require('../validations/locale-rules')

const router = express.Router()

router.get(
  '/',
  isAuthMiddleware,
  wrapAsync(localesController.findAll)
)
router.get(
  '/:id',
  isAuthMiddleware,
  localesValidation.findById,
  validatorMiddleware,
  wrapAsync(localesController.findById)
)
router.post(
  '/',
  isAuthMiddleware,
  localesValidation.create,
  validatorMiddleware,
  wrapAsync(localesController.create)
)
router.put(
  '/:id',
  isAuthMiddleware,
  localesValidation.update,
  validatorMiddleware,
  wrapAsync(localesController.update)
)
router.delete(
  '/:id',
  isAuthMiddleware,
  localesValidation.delete,
  validatorMiddleware,
  wrapAsync(localesController.delete)
)

module.exports = router
