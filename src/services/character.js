const axios = require('axios')

const BASE_API_URL = 'https://rickandmortyapi.com/api'

module.exports = {
  findCharacters: async (page = 1) => {
    return axios({
      method: 'get',
      url: `${BASE_API_URL}/character/?page=${page}`,
      json: true
    })
      .then(result => result.data)
  }
}
