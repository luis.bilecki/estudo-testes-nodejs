const jwt = require('jsonwebtoken')

const { JWT_SECRET } = process.env

const signOptions = {
  issuer: 'Test Luis',
  audience: 'test.luis.com',
  expiresIn: '1 days'
}

const verifyOptions = {
  issuer: 'Test Luis',
  audience: 'test.luis.com'
}

const sign = email => {
  return jwt.sign(
    { email },
    JWT_SECRET,
    signOptions
  )
}

const validate = token => {
  return jwt.verify(token, JWT_SECRET, verifyOptions)
}

module.exports = {
  sign,
  validate
}
