const express = require('express')
const app = express()

const authRoutes = require('./routes/auth')
const localeRoutes = require('./routes/locale')
const characterRoutes = require('./routes/character')

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/auth', authRoutes)
app.use('/locales', localeRoutes)
app.use('/characters', characterRoutes)

app.use(require('./errors/error-handler'))

module.exports = app
