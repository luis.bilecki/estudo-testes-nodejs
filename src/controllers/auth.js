const UserModel = require('../models/user')

const DefaultError = require('../errors/DefaultError')

const jwtService = require('../services/jwt')

module.exports = {
  signUp: async (req, res, next) => {
    const { email, password, name } = req.body

    const user = new UserModel({
      name,
      email,
      password
    })

    await user.save()

    return res.json({ success: true })
  },
  signIn: async (req, res, next) => {
    const { email, password } = req.body

    const user = await UserModel.findOne({ email })

    if (!user) {
      return next(DefaultError.notFound(req, 'User not found'))
    }
    const passwordIsWrong = !user.comparePassword(password)

    if (passwordIsWrong) {
      return next(DefaultError.unAuthorized(req, 'Wrong password!'))
    }

    const issuedToken = jwtService.sign(email)

    return res.json({ success: true, token: issuedToken })
  },
  validateToken: async (req, res, next) => {
    const { token } = req.body

    try {
      await jwtService.validate(token)

      res.json({ status: 'valid' })
    } catch (err) {
      return next(DefaultError.unAuthorized(req, 'Invalid token'))
    }
  }
}
