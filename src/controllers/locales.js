const LocaleModel = require('../models/locale')

const DefaultError = require('../errors/DefaultError')

module.exports = {
  create: async (req, res, next) => {
    const { name, type, coordinates } = req.body

    const locale = new LocaleModel({
      name,
      geometry: {
        type,
        coordinates
      }
    })

    await locale.save()

    return res.json(locale)
  },
  findAll: async (req, res, next) => {
    const locales = await LocaleModel.find()
    return res.json(locales)
  },
  findById: async (req, res, next) => {
    const { id } = req.params
    const locale = await LocaleModel.findById(id)
    const localeNotFound = !locale

    if (localeNotFound) {
      return next(DefaultError.notFound(req, 'Locale not found'))
    }

    return res.json(locale)
  },
  delete: async (req, res, next) => {
    const { id } = req.params

    await LocaleModel.findOneAndDelete({ _id: id })

    return res.status(204).json()
  },
  update: async (req, res, next) => {
    const { name, coordinates } = req.body
    const { id } = req.params

    const locale = await LocaleModel.findById(id)
    const localeNotFound = !locale

    if (localeNotFound) {
      return next(DefaultError.notFound(req, 'Locale not found'))
    }

    locale.name = name
    locale.geometry.coordinates = coordinates

    await locale.save()

    return res.json(locale)
  }
}
