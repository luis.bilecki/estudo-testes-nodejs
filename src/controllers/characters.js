const characterService = require('../services/character')

module.exports = {
  findAll: async (req, res, next) => {
    const page = req.query.page
    const characters = await characterService.findCharacters(page)

    return res.json(characters)
  }
}
