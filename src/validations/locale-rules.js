const { body, param } = require('express-validator')

const coordinatesValidation = body('coordinates')
  .custom(coords => Array.isArray(coords))
  .withMessage('An array is required')

module.exports = {
  create: [
    body('name')
      .not()
      .isEmpty(),
    body('type').isIn(['Point']),
    coordinatesValidation
  ],
  findById: [
    param('id').isMongoId()
  ],
  delete: [
    param('id').isMongoId()
  ],
  update: [
    param('id').isMongoId(),
    body('name')
      .not()
      .isEmpty(),
    coordinatesValidation
  ]
}
