const { body } = require('express-validator')

module.exports = {
  signIn: [
    body('email').isEmail(),
    body('password')
      .not()
      .isEmpty()
  ],
  signUp: [
    body('email').isEmail(),
    body('password').isLength({ min: 8 })
  ],
  validateToken: [
    body('token')
      .not()
      .isEmpty()
  ]
}
