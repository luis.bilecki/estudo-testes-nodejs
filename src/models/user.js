const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SALT_ROUNDS = 10

const userSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String
  },
  enabled: {
    type: Boolean,
    default: true
  }
})

userSchema.pre('save', async function (next) {
  const user = this

  const passwordChanged = user.isModified('password')

  if (passwordChanged) {
    const salt = bcrypt.genSaltSync(SALT_ROUNDS)
    user.password = bcrypt.hashSync(user.password, salt)
  }

  next()
})

userSchema.methods.comparePassword = function (providedPassword) {
  const user = this
  return bcrypt.compareSync(providedPassword, user.password)
}

module.exports = mongoose.model('User', userSchema)
